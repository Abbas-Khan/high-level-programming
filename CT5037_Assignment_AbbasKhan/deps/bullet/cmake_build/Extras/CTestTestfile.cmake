# CMake generated Testfile for 
# Source directory: C:/Users/AbbasKhan/Desktop/TempBackup/ComponentTutorial/deps/bullet/bullet3-2.87/Extras
# Build directory: C:/Users/AbbasKhan/Desktop/TempBackup/ComponentTutorial/deps/bullet/cmake_build/Extras
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
subdirs("InverseDynamics")
subdirs("BulletRobotics")
subdirs("obj2sdf")
subdirs("Serialize")
subdirs("ConvexDecomposition")
subdirs("HACD")
subdirs("GIMPACTUtils")
