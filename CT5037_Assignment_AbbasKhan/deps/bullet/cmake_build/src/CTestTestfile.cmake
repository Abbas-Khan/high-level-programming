# CMake generated Testfile for 
# Source directory: C:/Users/AbbasKhan/Desktop/TempBackup/ComponentTutorial/deps/bullet/bullet3-2.87/src
# Build directory: C:/Users/AbbasKhan/Desktop/TempBackup/ComponentTutorial/deps/bullet/cmake_build/src
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
subdirs("Bullet3OpenCL")
subdirs("Bullet3Serialize/Bullet2FileLoader")
subdirs("Bullet3Dynamics")
subdirs("Bullet3Collision")
subdirs("Bullet3Geometry")
subdirs("BulletInverseDynamics")
subdirs("BulletSoftBody")
subdirs("BulletCollision")
subdirs("BulletDynamics")
subdirs("LinearMath")
subdirs("Bullet3Common")
