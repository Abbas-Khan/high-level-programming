# CMake generated Testfile for 
# Source directory: C:/Users/AbbasKhan/Desktop/TempBackup/ComponentTutorial/deps/bullet/bullet3-2.87/test
# Build directory: C:/Users/AbbasKhan/Desktop/TempBackup/ComponentTutorial/deps/bullet/cmake_build/test
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
subdirs("InverseDynamics")
subdirs("SharedMemory")
subdirs("gtest-1.7.0")
subdirs("collision")
subdirs("BulletDynamics")
