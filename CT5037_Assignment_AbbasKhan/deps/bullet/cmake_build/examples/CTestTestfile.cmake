# CMake generated Testfile for 
# Source directory: C:/Users/AbbasKhan/Desktop/TempBackup/ComponentTutorial/deps/bullet/bullet3-2.87/examples
# Build directory: C:/Users/AbbasKhan/Desktop/TempBackup/ComponentTutorial/deps/bullet/cmake_build/examples
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
subdirs("HelloWorld")
subdirs("BasicDemo")
subdirs("ExampleBrowser")
subdirs("RobotSimulator")
subdirs("SharedMemory")
subdirs("ThirdPartyLibs/Gwen")
subdirs("ThirdPartyLibs/BussIK")
subdirs("ThirdPartyLibs/clsocket")
subdirs("OpenGLWindow")
