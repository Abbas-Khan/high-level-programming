#ifndef _BOID_H
#define _BOID_H

///////////////////////////////////////////////////////////////////
// Name: Boid.h
// Author: Abbas Khan
// Bio: Entity Class to serve as the pinky Parent
///////////////////////////////////////////////////////////////////

// Include the necessary header files
#include "Entity.h"

// Boid Class inherits from Entity
class Boid : public Entity
{
public:
	// Constructor
	Boid();
	// Destructor
	~Boid();
	// Overridden Virtual Update function used to update
	virtual void Update(float a_fDeltaTime);
	// Overridden Virtual Draw function with used to draw
	virtual void Draw(unsigned int a_uProgramID, unsigned int a_uVBO, unsigned int a_uIBO);
protected:
	// Float used to determine the scale od the model
	float m_fScale;
};

#endif //_BOID_H