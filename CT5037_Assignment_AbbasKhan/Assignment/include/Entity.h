#ifndef _ENTITY_H
#define _ENTITY_H

///////////////////////////////////////////////////////////////////
// Name: Entity.h
// Author: Abbas Khan
// Bio: Used as the base class for entities within the E.C.S
///////////////////////////////////////////////////////////////////

// Include the necessary header files
#include <vector>
#include <map>
#include "Enumerations.h"

// Forward declaration of component class
class Component;

// Entity Class does not inherit
class Entity
{
public:
	// Constructor
	Entity();
	// Destructor
	~Entity();
	// Virtual Update Function with definition in CPP
	virtual void Update(float a_fDeltaTime);
	// Virtual Draw Function with definition in CPP
	virtual void Draw(unsigned int a_uProgramID, unsigned int a_uVBO, unsigned int a_uIBO);
	// Function to add a component to the entity's component vector
	void AddComponent(Component* a_Component) { m_apComponentVector.push_back(a_Component); }
	// Function to find a component within an entity's component vector
	Component* FindComponentOfType(COMPONENT_TYPE a_eComponentType);
	// Function to get the entity's ID
	unsigned int GetEntityID() { return m_uEntityID; }
	// Function to add the entity to the correct maps
	void Entity::AddToMap(unsigned int a_num, Entity* a_Entity, bool a_isboid);
	// Static functions to return the entire entity map, just the boids or just the obstacles
	static std::map<const unsigned int, Entity*>GetEntityMap() { return s_xEntityMap; }
	static std::map<const unsigned int, Entity*>GetBoidMap() { return s_xBoidMap; }
	static std::map<const unsigned int, Entity*>GetObstacleMap() { return s_xObstacleMap; }
private:
	// unsigned int used to hold the entity's unique ID
	unsigned int m_uEntityID;
	// vector used to hold all the entity's components
	std::vector<Component*> m_apComponentVector;
	// Static unsigned int used to keep track of the amount of entities
	static unsigned int s_uEntityIDCount;
	// Static maps to hold all the entities, just the boids or just the obstacles
	static std::map<const unsigned int, Entity*> s_xEntityMap;
	static std::map<const unsigned int, Entity*> s_xBoidMap;
	static std::map<const unsigned int, Entity*> s_xObstacleMap;
};

#endif // _ENTITY_H