#ifndef _AUDIO_H
#define _AUDIO_H

///////////////////////////////////////////////////////////////////
// Name: Audio.h
// Author: Abbas Khan
// Bio: Component Class used to control the Audio
///////////////////////////////////////////////////////////////////

// Include the necessary header files
#include "Component.h"
#include "fmod.hpp"

// Audio Class inherits from Component
class Audio : public Component
{
public:
	// Constructor
	Audio(Entity* a_OwnerEntity);
	// Destructor
	~Audio();
	// Overridden Virtual Update function used to update the state of the component
	virtual void Update(float a_fDeltaTime);
	// Overridden Virtual Draw function with no implementation as it's not necessary for this component
	virtual void Draw(unsigned int a_uProgramID, unsigned int a_uVBO, unsigned int a_uIBO) {};
	// Create sound function used to create the FMOD sound
	void CreateSound(const char *name_or_data, FMOD_MODE mode, FMOD_CREATESOUNDEXINFO *exinfo, FMOD::Sound **sound);
	// Set mode function used to set the FMOD mode
	void SetMode(FMOD_MODE mode);
	// Play sound function used to play the FMOD sound
	void PlaySound(FMOD::Sound *sound, FMOD::ChannelGroup *channelgroup, bool paused, FMOD::Channel **channel);
private:
	// FMOD sound variables
	FMOD::System* m_FMOD_System;
	FMOD::Sound* m_FMOD_Sound;
	FMOD::Channel* m_FMOD_Channel = 0;
	// Boolean to determine whether audio is played or not
	bool b_FMOD_Played = false;
	// Timers used to sync audio and prevent overlapping sound
	float InitialfTimer = 0;
	float fTimer = 1;
};

#endif //_AUDIO_H