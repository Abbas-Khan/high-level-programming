#ifndef _ENUMERATIONS_H
#define _ENUMERATIONS_H

///////////////////////////////////////////////////////////////////
// Name: Enumerations.h
// Author: Abbas Khan
// Bio: Header file used to hold all the enumerations
///////////////////////////////////////////////////////////////////

// Enum used to store the different component types
enum COMPONENT_TYPE
{
	TRANSFORM,
	MODEL,
	BRAIN,
	COLLISION,
	AUDIO,
	PRIMITVE
};

// Enum used to store the different parts of the matrix
enum MATRIX_ROW
{
	RIGHT_VECTOR,
	UP_VECTOR,
	FORWARD_VECTOR,
	POSITION_VECTOR,
};

#endif //_ENUMERATIONS_H