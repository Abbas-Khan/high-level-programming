#ifndef _COMPONENT_H
#define _COMPONENT_H

///////////////////////////////////////////////////////////////////
// Name: Component.h
// Author: Abbas Khan
// Bio: Used as the base class for all the components in the E.C.S
///////////////////////////////////////////////////////////////////

// Include the necessary header files
#include "Entity.h"
#include "Enumerations.h"

// Component Class
class Component
{
public:
	// Constructor
	Component(Entity* a_OwnerEntity);
	// Destructor
	~Component();
	// Virtual update function that has to be overridden in any class that inherits this class
	virtual void Update(float a_fDeltaTime) = 0;
	// Virtual Draw function that has to be overridden in any class that inherits this class
	virtual void Draw(unsigned int a_uProgramID, unsigned int a_uVBO, unsigned int a_uIBO) = 0;
	// Inline function used to get the component type of this component
	COMPONENT_TYPE GetComponentType() { return m_eComponentType; }
	// Inline function used to get the owner entity of this component
	Entity* GetOwnerEntity() { return m_pOwnerEntity; }
protected:
	// Entity pointer used to hold a reference to the owner entity
	Entity* m_pOwnerEntity;
	// Component Type used to determine which component is this object
	COMPONENT_TYPE m_eComponentType;
};

#endif //_COMPONENT_H