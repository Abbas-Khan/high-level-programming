#ifndef _CONSTANTS_H
#define _CONSTANTS_H

///////////////////////////////////////////////////////////////////
// Name: Constants.h
// Author: Abbas Khan
// Bio: Header file used to hold all the constants
///////////////////////////////////////////////////////////////////

// #defines for screen width and height
#define DEFAULT_SCREENWIDTH 1024
#define DEFAULT_SCREENHEIGHT 720
// Constant integer for max number of boids
const int iMAX_NUM_OF_BOIDS = 20;

#endif //_CONSTANTS_H