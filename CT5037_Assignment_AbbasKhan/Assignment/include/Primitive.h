#ifndef _PRIMITIVE_H
#define _PRIMITIVE_H

///////////////////////////////////////////////////////////////////
// Name: Primitive.h
// Author: Abbas Khan
// Bio: Component Class used to control the rendering of a primitive
///////////////////////////////////////////////////////////////////

// Include the necessary header files
#include "Component.h"

// Primitive Class inherits from Component
class Primitive : public Component
{
public:
	// Constructor
	Primitive(Entity* a_OwnerEntity);
	// Destructor
	~Primitive();
	// Overridden Virtual Update function used to update the state of the component
	virtual void Update(float a_fDeltaTime);
	// Overridden Virtual Draw function with no implementation as it's not necessary for this component
	virtual void Draw(unsigned int a_uProgramID, unsigned int a_uVBO, unsigned int a_uIBO) {};
};

#endif //_PRIMITIVE_H