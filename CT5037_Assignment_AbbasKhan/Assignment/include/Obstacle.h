#ifndef _OBSTACLE_H
#define _OBSTACLE_H

///////////////////////////////////////////////////////////////////
// Name: Obstacle.h
// Author: Abbas Khan
// Bio: Entity Class to serve as the obstacles within the project
///////////////////////////////////////////////////////////////////

// Include the necessary header files
#include "Entity.h"
#include "glm\ext.hpp"

// Obstacle Class inherits from Entity
class Obstacle : public Entity
{
public:
	// Constructor
	Obstacle();
	// Destructor
	~Obstacle();
	// Overridden Virtual Update function used to update the obstacle
	virtual void Update(float a_fDeltaTime);
	// Overridden Virtual Draw function with used to draw the obstacle
	virtual void Draw(unsigned int a_uProgramID, unsigned int a_uVBO, unsigned int a_uIBO);
	// Function used to set the obstacle's position
	void SetPosition(glm::vec3 TargetPosition);
};

#endif //_OBSTACLE_H