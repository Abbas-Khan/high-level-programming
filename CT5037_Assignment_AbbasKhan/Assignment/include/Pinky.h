#ifndef _PINKY_H
#define _PINKY_H

///////////////////////////////////////////////////////////////////
// Name: Pinky.h
// Author: Abbas Khan
// Bio: Boid Class to serve as the boids within the project
///////////////////////////////////////////////////////////////////

// Include the necessary header files
#include "Boid.h"
#include "Model.h"
#include "Entity.h"

// Pinku Class inherits from Boid
class Pinky : public Boid
{
public:
	// Constructor
	Pinky();
	// Destructor
	~Pinky();
	// Overridden Virtual Update function used to update the obstacle
	virtual void Update(float a_fDeltaTime);
	// Overridden Virtual Draw function with used to draw the obstacle
	virtual void Draw(unsigned int a_uProgramID, unsigned int a_uVBO, unsigned int a_uIBO);
	// Function used to load the model
	void LoadModel();
};

#endif //_PINKY_H