#ifndef _BRAIN_H
#define _BRAIN_H

///////////////////////////////////////////////////////////////////
// Name: Brain.h
// Author: Abbas Khan
// Bio: Component Class used to control the behaviour of the AI
///////////////////////////////////////////////////////////////////

// Include the necessary header files
#include "Component.h"
#include <glm\glm.hpp>

// Brain Class inherits from Component
class Brain : public Component
{
public:
	// Constructor
	Brain(Entity* a_OwnerEntity);
	// Destructor
	~Brain();
	// Overridden Virtual Update function used to update the state of the component
	virtual void Update(float a_fDeltaTime);
	// Overridden Virtual Draw function with no implementation as it's not necessary for this component
	virtual void Draw(unsigned int a_uProgramID, unsigned int a_uVBO, unsigned int a_uIBO) {};
	// Inline function to get the current velocity
	inline glm::vec3 GetCurrentVelocity() { return m_v3CurrentVelocity; }
	// Selection of public float variables used to multiply the forces
	float Wander_Multiplier = 0.0f;
	float Seperation_Multiplier = 0.0f;
	float Alignment_Multiplier = 0.0f;
	float Cohesion_Multiplier = 0.0f;
	float ObstacleAvoidance_Multiplier = 0.0f;
	// Static floats used to control the boids steering
	static float Max_Speed;
	static float Jitter;
	static float Wander_Radius;
	static float Circle_Forward_Multiplier;
private:
	// Steering Behaviours - (Individual)
	glm::vec3 CalculateSeekForce(const glm::vec3& v3Target, const glm::vec3& v3CurrentPos) const;
	glm::vec3 CalculateFleeForce(const glm::vec3& v3Target, const glm::vec3& v3CurrentPos) const;
	glm::vec3 CalculateWanderForce(const glm::vec3& v3CurrentPos, const glm::vec3& v3Forward) const;
	glm::vec3 CalculateEvadeForce(Entity* EntityTarget, const glm::vec3& v3CurrentPos) const;
	glm::vec3 CalculatePursueForce(Entity* EntityTarget, const glm::vec3& v3CurrentPos) const;
	glm::vec3 CalculatePathFollowingForce(glm::vec3 PathArray[], const glm::vec3& v3CurrentPos);
	glm::vec3 CalculateContainmentForce(const glm::vec3& v3CurrentPos);
	glm::vec3 CalculateObstacleAvoidanceForce(const glm::vec3& v3CurrentPos);
	glm::vec3 CalculateArrivalForce(const glm::vec3& v3Target, const glm::vec3& v3CurrentPos);
	// Steering Behaviours - (Group)
	glm::vec3 CalculateSeperationForce();
	glm::vec3 CalculateAlignmentForce();
	glm::vec3 CalculateCohesionForce();
	// Vector 3 used to hold the current velocity of the AI
	glm::vec3 m_v3CurrentVelocity;
	// Float used to stop the AI from getting stuck on the border
	float m_fContainmentTimer;
	// Unsigned int used to iterate over the path in path following behaviour
	unsigned int PathIterator = 0;
};

#endif //_BRAIN_H