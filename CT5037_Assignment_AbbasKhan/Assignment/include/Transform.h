#ifndef _TRANSFORM_H
#define _TRANSFORM_H

///////////////////////////////////////////////////////////////////
// Name: Transform.h
// Author: Abbas Khan
// Bio: Component Class used to control the transform of the AI
///////////////////////////////////////////////////////////////////

// Include the necessary header files
#include "Component.h"
#include "glm\ext.hpp"

// Transform Class inherits from Component
class Transform : public Component
{
public:
	// Constructor
	Transform(Entity* a_OwnerEntity);
	// Destructor
	~Transform();
	// Overridden Virtual Draw function with no implementation as it's not necessary for this component
	virtual void Update(float a_fDeltaTime) {};
	// Overridden Virtual Draw function with no implementation as it's not necessary for this component
	virtual void Draw(unsigned int a_uProgramID, unsigned int a_uVBO, unsigned int a_uIBO) {};
	// Inline functions used to get various positional data from the entity matrix
	inline glm::vec3 GetRightDirection() { return GetEntityMatrixRow(RIGHT_VECTOR); }
	inline glm::vec3 GetUpDirection() { return GetEntityMatrixRow(UP_VECTOR); }
	inline glm::vec3 GetFacingDirection() { return GetEntityMatrixRow(FORWARD_VECTOR); }
	inline glm::vec3 GetCurrentPosition() { return GetEntityMatrixRow(POSITION_VECTOR); }
	// Inline functions used to set various positional data of the entity matrix
	inline void SetRightDirection(glm::vec3 vRightDirection) { SetEntityMatrixRow(RIGHT_VECTOR, vRightDirection); }
	inline void SetUpDirection(glm::vec3 vUpDirection) { SetEntityMatrixRow(UP_VECTOR, vUpDirection); }
	inline void SetFacingDirection(glm::vec3 vFacingDirection) { SetEntityMatrixRow(FORWARD_VECTOR, vFacingDirection); }
	inline void SetCurrentPosition(glm::vec3 vCurrentPosition) { SetEntityMatrixRow(POSITION_VECTOR, vCurrentPosition); }
	// Public const Inline function used to get the entity matrix
	inline const glm::mat4& GetEntityMatrix() { return m_m4EntityMatrix; }

private:
	// Inline function used to set the data in a specific row of the entity matrix
	inline void SetEntityMatrixRow(MATRIX_ROW eRow, glm::vec3 vVec) { m_m4EntityMatrix[eRow] = glm::vec4(vVec, (eRow == POSITION_VECTOR ? 1.f : 0.f)); }
	// Private Inline function used to get the entity matrix
	inline glm::vec3 GetEntityMatrixRow(MATRIX_ROW eRow) { return m_m4EntityMatrix[eRow]; }
	// Mat4 matrix used to hold the entity's positional data
	glm::mat4 m_m4EntityMatrix;
};

#endif //_BRAIN_H