#ifndef _GAME_H
#define _GAME_H

///////////////////////////////////////////////////////////////////
// Name: Game.cpp
// Author: Abbas Khan
// Bio: Manager class for the entire project
///////////////////////////////////////////////////////////////////

// Include the necessary header files
#include "Application.h"
#include "Constants.h"
#include "glm/glm.hpp"
#include "MD5Model.h"
#include <vector>
#include "btBulletCollisionCommon.h"
#include "btBulletDynamicsCommon.h"

// Forward Declarations
class Boid;
class Entity;

// Game Class inherits from application
class Game : public Application
{
public:
	// Constructor
	Game();
	// Destructor
	~Game();
	// Static function to retrieve the game instance
	static Game& GetGame();
	// Inline functions to get the IBO, VAO< VBO & ProgramID
	inline unsigned int GetIBO() { return m_ibo; }
	inline unsigned int GetVAO() { return m_vao; }
	inline unsigned int GetVBO() { return m_vbo; }
	inline unsigned int GetProgramID() { return m_programID; }
	// Inline function to retrieve the dynamic physics world
	inline btDynamicsWorld* GetDynamicWorld() { return m_pWorld; }
protected:
	// OnCreate boolean function
	virtual bool onCreate();
	// Update function
	virtual void Update(float a_deltaTime);
	// Draw function
	virtual void Draw();
	// Destroy function
	virtual void Destroy();
private:
	// Static Game variable
	static Game* xGameInstance;
	// Function to create a number of boids
	void CreateBoids();
	// Function to randomly spawn an obstacle
	void CreateObstacleRandom();
	// Function to spawn an obstacle at a given location
	void CreateObstacle(int a_x, int a_z);
	// Unsigned ints for the VAO, VBO, IBO & Program ID
	unsigned int m_vao;
	unsigned int m_vbo;
	unsigned int m_ibo;
	unsigned int m_programID;
	// Unsigned ints for the vertex & fragment shaders
	unsigned int m_vertexShader;
	unsigned int m_fragmentShader;
	// Matrix4 for the camera 
	glm::mat4 m_cameraMatrix;
	// Matrix4 for the projection matrix
	glm::mat4 m_projectionMatrix;
	// Matrix4 for the light
	glm::vec4 m_lightPos;
	// Physics
	btDynamicsWorld* m_pWorld;
	btDispatcher* m_pDispatcher;
	btCollisionConfiguration* m_pCollisionConfig;
	btBroadphaseInterface* m_pBroadPhase;
	btConstraintSolver* m_pSolver;
};

#endif //_GAME_H