#ifndef _MODEL_H
#define _MODEL_H

///////////////////////////////////////////////////////////////////
// Name: Model.h
// Author: Abbas Khan
// Bio: Component Class used to control the rendering of the AI
///////////////////////////////////////////////////////////////////

// Include the necessary header files
#include "Component.h"
#include "MD5Model.h"
#include <vector>

// Model Class inherits from Component
class Model : public Component
{
public:
	// Constructor
	Model(Entity* a_OwnerEntity);
	// Destructor
	~Model();
	// Overridden Virtual Update function used to update the state of the component
	virtual void Update(float a_fDeltaTime);
	// Overridden Virtual Draw function used to draw the model
	virtual void Draw(unsigned int a_uProgramID, unsigned int a_uVBO, unsigned int a_uIBO);
	// Boolean function used to load a MD5 Model
	bool LoadModel(const char* a_szModelPath, float a_fScale);
	// Boolean Function used to load Animations
	bool LoadAnim(const unsigned int a_iNumberOfAnims, const char ** a_aAnimPaths);
	// Public const Inline function used to set the animation
	inline void SetCurrentAnim(const char* a_szCurrentAnimation) { m_szCurrentAnimation = a_szCurrentAnimation; }
	// Public const Inline function used to get the MD5 Model
	inline const MD5Model& GetModel() { return m_xModel; }
	// Public const Inline function used to get the Current Animation
	inline const char* GetCurrentAnim() { return m_szCurrentAnimation; }
private:
	// Private MD5Model variable to hold the model
	MD5Model m_xModel;
	// Const char to hold the current animation
	const char* m_szCurrentAnimation;
	// Float used to play the animations
	float m_fAnimTime;
};

#endif //_MODEL_H