#ifndef _COLLISION_H
#define _COLLISION_H

///////////////////////////////////////////////////////////////////
// Name: Collision.h
// Author: Abbas Khan
// Bio: Component Class used to control the behaviour of the AI
///////////////////////////////////////////////////////////////////

// Include the necessary header files
#include "Component.h"
#include "btBulletCollisionCommon.h"
#include "btBulletDynamicsCommon.h"

// Collision class inherits from component
class Collision : public Component
{
public:
	// Constructor which takes entity as an arguement
	Collision(Entity* A_pOwner);
	// Destructor
	~Collision();
	// Overridden Virtual Update function used to update the state of the component
	virtual void Update(float a_fDeltaTime);
	// Overridden Virtual Draw function with no implementation as it's not necessary for this component
	virtual void Draw(unsigned int a_uProgramID, unsigned int a_uVBO, unsigned int a_uIBO) {};
	// biRigidBody function used to get the rigid body
	btRigidBody* GetRigidBody() { return m_pRigidBody; }
	// Variable used to tell when the transform can be controled by the rigidbody
	bool CanUpdate = false;
private:
	// Function used to create the rigidbody
	void CreateRigidBody();
	// Variable used to hold the rigid body
	btRigidBody* m_pRigidBody;
	// Timer used to prevent all objects snapping to 0,0,0 on spawn
	float Ftimer = 10.0f;
};

#endif //_COLLISION_H