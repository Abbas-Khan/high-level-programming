///////////////////////////////////////////////////////////////////
// Name: Model.cpp
// Author: Abbas Khan
// Bio: Component Class used to control the rendering of the AI
///////////////////////////////////////////////////////////////////

// Include the necessary header files
#include "Model.h"
#include <GL\glew.h>
#include "Transform.h"
#include "glm\ext.hpp"

// Typedef component as parent
typedef Component Parent;

// Static const float used for the anim playback rate
static const float ANIM_PLABACL_RATE = 2.0f;

// Constructor
Model::Model(Entity* a_OwnerEntity)
	: Parent(a_OwnerEntity), m_fAnimTime(0.0f)
{
	// Set the component type
	m_eComponentType = MODEL;
}

// Destructor
Model::~Model()
{
}

// Update
void Model::Update(float a_fDeltaTime)
{
	// Increment anim time
	m_fAnimTime += a_fDeltaTime * ANIM_PLABACL_RATE;
	// Evaluate the animation
	m_xModel.evaluateAnimation(m_szCurrentAnimation, m_fAnimTime, true);
	// Update the bones
	m_xModel.UpdateBones();
}

// Draw
void Model::Draw(unsigned int a_uProgramID, unsigned int a_uVBO, unsigned int a_uIBO)
{
	// Bind the array of bones
	MD5Skeleton* skeleton = m_xModel.m_skeleton;
	int boneLocation = glGetUniformLocation(a_uProgramID, "Bones");
	glUniformMatrix4fv(boneLocation, skeleton->m_jointCount, GL_FALSE, (float*)skeleton->m_bones);

	// Extract the transform component
	Transform* TransformComponent = static_cast<Transform*>(m_pOwnerEntity->FindComponentOfType(TRANSFORM));

	// Null check
	if (!TransformComponent)
	{
		return;
	}

	const glm::mat4* pEntityMatrix = &TransformComponent->GetEntityMatrix();
	if (!pEntityMatrix) return; // Early out if no matrix

								// Draw MD5 Model
								// for each mesh in the model...
	for (unsigned int i = 0; i < m_xModel.getMeshCount(); ++i)
	{
		// get the current mesh
		MD5Mesh* pMesh = m_xModel.m_meshes[i];

		unsigned int modelUniform = glGetUniformLocation(a_uProgramID, "Model");
		glUniformMatrix4fv(modelUniform, 1, false, glm::value_ptr(*pEntityMatrix));

		// Bind the texture to one of the ActiveTextures
		//bind our textureLocation variable from the shaders and set it's value to 0 as the active texture is texture 0
		unsigned int texUniformID = glGetUniformLocation(a_uProgramID, "DiffuseTexture");
		glUniform1i(texUniformID, 0);

		//set active texture, and bind loaded texture
		glActiveTexture(GL_TEXTURE0);
		glBindTexture(GL_TEXTURE_2D, pMesh->m_material.textureIDs[MD5Material::DiffuseTexture]);

		texUniformID = glGetUniformLocation(a_uProgramID, "NormalMap");
		glUniform1i(texUniformID, 1);

		//set active texture, and bind loaded texture
		glActiveTexture(GL_TEXTURE1);
		glBindTexture(GL_TEXTURE_2D, pMesh->m_material.textureIDs[MD5Material::NormalTexture]);

		texUniformID = glGetUniformLocation(a_uProgramID, "SpecularMap");
		glUniform1i(texUniformID, 2);

		//set active texture, and bind loaded texture
		glActiveTexture(GL_TEXTURE2);
		glBindTexture(GL_TEXTURE_2D, pMesh->m_material.textureIDs[MD5Material::SpecularTexture]);

		// Send the vertex data to the VBO
		glBindBuffer(GL_ARRAY_BUFFER, a_uVBO);
		glBufferData(GL_ARRAY_BUFFER, pMesh->m_vertices.size() * sizeof(MD5Vertex), pMesh->m_vertices.data(), GL_STATIC_DRAW);

		// send the index data to the IBO
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, a_uIBO);
		glBufferData(GL_ELEMENT_ARRAY_BUFFER, pMesh->m_indices.size() * sizeof(unsigned int), pMesh->m_indices.data(), GL_STATIC_DRAW);

		glDrawElements(GL_TRIANGLES, (GLsizei)pMesh->m_indices.size(), GL_UNSIGNED_INT, 0);
	}
}

// Load model
bool Model::LoadModel(const char * a_szModelPath, float a_fScale)
{
	// Depending on the result of load model
	if (m_xModel.Load(a_szModelPath, a_fScale))
	{
		return true;
	}
	else
	{
		return false;
	}
}

// Load anim
bool Model::LoadAnim(const unsigned int a_iNumberOfAnims, const char ** a_aAnimPaths)
{
	// Depending on the result of load anim
	if (m_xModel.LoadAnims(a_iNumberOfAnims, a_aAnimPaths))
	{
		return true;
	}
	else
	{
		return false;
	}
}
