///////////////////////////////////////////////////////////////////
// Name: Entity.cpp
// Author: Abbas Khan
// Bio: Used as the base class for entities within the E.C.S
///////////////////////////////////////////////////////////////////

// Include the necessary header files
#include "Entity.h"
#include "Component.h"
#include "Model.h"
#include "Brain.h"
#include "Audio.h"
#include "Transform.h"
#include "Primitive.h"
#include "Collision.h"

// Static variable local definitions
unsigned int Entity::s_uEntityIDCount;
std::map<const unsigned int, Entity*> Entity::s_xEntityMap;
std::map<const unsigned int, Entity*> Entity::s_xBoidMap;
std::map<const unsigned int, Entity*> Entity::s_xObstacleMap;

// Constructor
Entity::Entity()
{
}

// Destructor
Entity::~Entity()
{
}

// Update
void Entity::Update(float a_fDeltaTime)
{
	// Vector iterator used to loop through the component vector
	std::vector< Component* >::iterator xIter;
	// for loop
	for (xIter = m_apComponentVector.begin(); xIter < m_apComponentVector.end(); ++xIter)
	{
		// Set the current component equal to the pointer
		Component* pCurrentComponent = *xIter;
		// Component type variable used to get the current component's type
		COMPONENT_TYPE eType = pCurrentComponent->GetComponentType();
		// If the type is correct
		if (eType == MODEL)
		{
			// Extract the component
			Model* TargetComp = static_cast<Model*>(pCurrentComponent);
			// If it exists
			if (TargetComp)
			{
				// Call update
				TargetComp->Update(a_fDeltaTime);
			}
		}
		else if (eType == BRAIN)
		{
			// Extract the component
			Brain* TargetComp = static_cast<Brain*>(pCurrentComponent);
			// If it exists
			if (TargetComp)
			{
				// Call update
				TargetComp->Update(a_fDeltaTime);
			}
		}
		else if (eType == COLLISION)
		{
			// Extract the component
			Collision* TargetComp = static_cast<Collision*>(pCurrentComponent);
			// If it exists
			if (TargetComp)
			{
				// Call update
				TargetComp->Update(a_fDeltaTime);
			}
		}
		else if (eType == TRANSFORM)
		{
			// Extract the component
			Transform* TargetComp = static_cast<Transform*>(pCurrentComponent);
			// If it exists
			if (TargetComp)
			{
				// Call update
				TargetComp->Update(a_fDeltaTime);
			}
		}
		else if (eType == AUDIO)
		{
			// Extract the component
			Audio* TargetComp = static_cast<Audio*>(pCurrentComponent);
			// If it exists
			if (TargetComp)
			{
				// Call update
				TargetComp->Update(a_fDeltaTime);
			}
		}
		else if (eType == PRIMITVE)
		{
			// Extract the component
			Primitive* TargetComp = static_cast<Primitive*>(pCurrentComponent);
			// If it exists
			if (TargetComp)
			{
				// Call update
				TargetComp->Update(a_fDeltaTime);
			}
		}
	}
}

// Draw
void Entity::Draw(unsigned int a_uProgramID, unsigned int a_uVBO, unsigned int a_uIBO)
{
	// Vector iterator used to loop through the component vector
	std::vector< Component* >::iterator xIter;
	// for loop
	for (xIter = m_apComponentVector.begin(); xIter < m_apComponentVector.end(); ++xIter)
	{
		// Set the current component equal to the pointer
		Component* pCurrentComponent = *xIter;
		// Component type variable used to get the current component's type
		COMPONENT_TYPE eType = pCurrentComponent->GetComponentType();
		// If the type is correct
		if (eType == MODEL)
		{
			// Extract the model component
			Model* pTargetComp = static_cast<Model*>(pCurrentComponent);
			// If it exists
			if (pTargetComp)
			{
				// Call the draw function
				pTargetComp->Draw(a_uProgramID, a_uVBO, a_uIBO);
			}
		}
	}
}

// Add to map function
void Entity::AddToMap(unsigned int a_num, Entity* a_Entity, bool a_isboid)
{
	// Set the entity's unique ID
	m_uEntityID = a_num;
	// Increase the entity counter
	s_uEntityIDCount = s_uEntityIDCount + 1;
	// Add this entity to the static entity map
	s_xEntityMap.insert(std::make_pair(a_num, a_Entity));
	// Depending on whether this entity is a boid or not
	if (a_isboid == true)
	{
		// Add it to the boid map
		s_xBoidMap.insert(std::make_pair(a_num, a_Entity));
	}
	else
	{
		// Add it to the obstacle map
		s_xObstacleMap.insert(std::make_pair(a_num, a_Entity));
	}
}

// Find component of type function
Component* Entity::FindComponentOfType(COMPONENT_TYPE a_eComponentType)
{
	// Vector iterator used to loop through the component vector
	std::vector< Component* >::iterator xIter;
	// for loop
	for (xIter = m_apComponentVector.begin(); xIter < m_apComponentVector.end(); ++xIter)
	{
		// Set the current component equal to the pointer
		Component* pCurrentComponent = *xIter;
		// Component type variable used to get the current component's type
		COMPONENT_TYPE eType = pCurrentComponent->GetComponentType();
		// If the type is correct
		if (eType == a_eComponentType)
		{
			// Return the current component
			return pCurrentComponent;
		}
	}
	// If we get through the entire list and that component isn't found then throw a nullptr
	return nullptr;
}
