///////////////////////////////////////////////////////////////////
// Name: Primitive.cpp
// Author: Abbas Khan
// Bio: Component Class used to control the rendering of a primitive
///////////////////////////////////////////////////////////////////

// Include the necessary header files
#include "Primitive.h"
#include "Gizmos.h"
#include "Transform.h"

// Typedef component as parent
typedef Component Parent;

// Constructor
Primitive::Primitive(Entity* a_OwnerEntity)
	: Parent(a_OwnerEntity)
{
}

// Destructor
Primitive::~Primitive()
{
}

// Update function
void Primitive::Update(float a_fDeltaTime)
{
	// Extract the transform component
	Transform* TransformComponent = static_cast<Transform*>(m_pOwnerEntity->FindComponentOfType(TRANSFORM));

	// Null Check
	if (!TransformComponent)
	{
		return;
	}

	// Draw a box at the current position
	Gizmos::addBox(TransformComponent->GetCurrentPosition(), glm::vec3(5.0f, 5.0f, 5.0f), true, glm::vec4(0.f, 0.0f, 0.f, 1.f));
}