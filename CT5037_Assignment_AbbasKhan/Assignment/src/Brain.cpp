///////////////////////////////////////////////////////////////////
// Name: Brain.h
// Author: Abbas Khan
// Bio: Component Class used to control the behaviour of the AI
///////////////////////////////////////////////////////////////////

// Include the necessary header files
#include "Brain.h"
#include "Transform.h"
#include "Primitive.h"
#include "Gizmos.h"
#include <glm\ext.hpp>

// Typedef component as parent
typedef Component Parent;

// Set default values for the static variables
float Brain::Max_Speed = 5.5f;
float Brain::Jitter = 0.1f;
float Brain::Wander_Radius = 2.0f;
float Brain::Circle_Forward_Multiplier = 5.0f;

// Constructor
Brain::Brain(Entity* a_OwnerEntity)
	: Parent(a_OwnerEntity), m_v3CurrentVelocity(0.f,0.f,0.f)
{
	// Set component type
	m_eComponentType = BRAIN;
}

// Destructor
Brain::~Brain()
{
}

// Update
void Brain::Update(float a_fDeltaTime)
{
	
	#pragma region Initial Setup
	// Find the owner entity
	Entity* Owner = GetOwnerEntity();
	// Null check
	if (!Owner)
	{
		return;
	}
	// Extract the transform component
	Transform* TransformComponent = static_cast<Transform*>(Owner->FindComponentOfType(TRANSFORM));
	// Null check
	if (!TransformComponent)
	{
		return;
	}
	// Create a vector 3 for the entity's forward direction & another for current position
	glm::vec3 v3Forward = TransformComponent->GetFacingDirection();
	glm::vec3 v3CurrentPosition = TransformComponent->GetCurrentPosition();
	#pragma endregion

	#pragma region Movement Calculation
	// Vector 3 for the total force to be applied
	glm::vec3 v3TotalForce(0.0f, 0.0f, 0.0f);
	// Containment force used to keep the entity within bounds
	glm::vec3 v3ContainmentForce = CalculateContainmentForce(v3CurrentPosition);

	// If the containment force & timer are less than 0
	if (glm::length(v3ContainmentForce) <= 0.0f && m_fContainmentTimer <= 0.0f)
	{
		// Calculate the forces
		glm::vec3 v3WanderForce = CalculateWanderForce(v3CurrentPosition, v3Forward);
		glm::vec3 v3SeperationForce = CalculateSeperationForce();
		glm::vec3 v3AlignmentForce = CalculateAlignmentForce();
		glm::vec3 v3CohesionForce = CalculateCohesionForce();
		glm::vec3 v3ObstacleAvoidanceForce = CalculateObstacleAvoidanceForce(v3CurrentPosition);
		
		// Add the forces together into v3TotalForce & use the multiplier to give weights
		v3TotalForce = (v3WanderForce * Wander_Multiplier) +
			(v3SeperationForce * Seperation_Multiplier) +
			(v3AlignmentForce * Alignment_Multiplier) +
			(v3CohesionForce * Cohesion_Multiplier) +
			(v3ObstacleAvoidanceForce * ObstacleAvoidance_Multiplier);
	}
	// Otherwise
	else
	{
		// Decrement the timer
		m_fContainmentTimer -= a_fDeltaTime;
		// Set the totalforce equal to the containment force
		v3TotalForce = v3ContainmentForce;
	}
	#pragma endregion

	#pragma region Force Application
	// If the the wander multiplier is less than 0
	if (glm::length(Wander_Multiplier) <= 0.0f)
	{
		// Then stop the movement
		m_v3CurrentVelocity = glm::vec3(0.f, 0.f, 0.f);
	}
	// Otherwise
	else
	{
		// Add the total force to current velocity
		m_v3CurrentVelocity += v3TotalForce;
	}

	// Clamp the current velocity to stop the movement in Y & limit speed
	m_v3CurrentVelocity = glm::clamp(m_v3CurrentVelocity, glm::vec3(-10.f, 0.f, -10.f), glm::vec3(10.f, 0.f, 10.f));
	// Add the current velocity to the current position
	v3CurrentPosition += m_v3CurrentVelocity * a_fDeltaTime;

	// If the current velocity is greater than 0
	if (glm::length(m_v3CurrentVelocity) > 0.0f)
	{
		// Then normalise it
		v3Forward = glm::normalize(m_v3CurrentVelocity);
	}

	// Update the transform component
	glm::vec3 v3Up = TransformComponent->GetUpDirection();
	glm::vec3 v3Right = glm::cross(v3Up, v3Forward);
	TransformComponent->SetFacingDirection(v3Forward);
	TransformComponent->SetCurrentPosition(v3CurrentPosition);
	TransformComponent->SetRightDirection(v3Right);
	#pragma endregion	
	
}

// Calculate Seek Force
glm::vec3 Brain::CalculateSeekForce(const glm::vec3 & v3Target, const glm::vec3 & v3CurrentPos) const
{
	// Set the target direction and velocity
	glm::vec3 v3TargetDirection = glm::normalize(v3Target - v3CurrentPos);
	glm::vec3 v3NewVelocity = v3TargetDirection * Max_Speed;
	// Calcukate the force
	glm::vec3 v3Force = v3NewVelocity - m_v3CurrentVelocity;
	// Display the gizmos
	Gizmos::addCircle(v3Target, 2.0f, 16, false, glm::vec4(0.f, 0.8f, 0.2f, 1.0f));
	Gizmos::addLine(v3CurrentPos, v3Target, glm::vec4(1.0f, 0.0f, 0.0f, 1.0f));
	// Return the force
	return v3Force;
}

// Calculate Flee Force
glm::vec3 Brain::CalculateFleeForce(const glm::vec3 & v3Target, const glm::vec3 & v3CurrentPos) const
{
	// Set the target direction and velocity
	glm::vec3 v3TargetDirection = glm::normalize(v3CurrentPos - v3Target);
	glm::vec3 v3NewVelocity = v3TargetDirection * Max_Speed;
	// Calcukate the force
	glm::vec3 v3Force = v3NewVelocity - m_v3CurrentVelocity;
	// Display the gizmos
	Gizmos::addCircle(v3Target, 2.0f, 16, false, glm::vec4(0.f, 0.2f, 0.2f, 1.0f));
	Gizmos::addLine(v3Target, v3CurrentPos, glm::vec4(0.0f, 1.0f, 1.0f, 1.0f));
	// Return the force
	return v3Force;
}

// Calculate Wander Force
glm::vec3 Brain::CalculateWanderForce(const glm::vec3 & v3CurrentPos, const glm::vec3 & v3Forward) const
{
	// Generate a random point within a circle
	glm::vec3 v3WanderWanderCircleOrigin = v3CurrentPos + (v3Forward * Circle_Forward_Multiplier);
	glm::vec2 v2RandPoint = glm::circularRand(Wander_Radius);
	glm::vec3 v3RandPointOnCircle = v3WanderWanderCircleOrigin + glm::vec3(v2RandPoint.x, 0.0f, v2RandPoint.y);
	v2RandPoint = glm::circularRand(Jitter);
	glm::vec3 v3RandTarget = v3RandPointOnCircle + glm::vec3(v2RandPoint.x, 0.0f, v2RandPoint.y);
	glm::vec3 v3Target = v3WanderWanderCircleOrigin + (glm::normalize(v3RandTarget - v3RandPointOnCircle) * Wander_Radius);
	// Set the target direction and velocity
	glm::vec3 v3TargetDirection = glm::normalize(v3Target - v3CurrentPos);
	glm::vec3 v3NewVelocity = v3TargetDirection * Max_Speed;
	// Calcukate the force
	glm::vec3 v3Force = v3NewVelocity - m_v3CurrentVelocity;
	// Display the gizmos
	Gizmos::addCircle(v3WanderWanderCircleOrigin, Wander_Radius, 16, false, glm::vec4(1.0f, 0.0f, 0.0f, 1.0f));
	// Return the force
	return v3Force;
}

// Calculate Evade Force
glm::vec3 Brain::CalculateEvadeForce(Entity* EntityTarget, const glm::vec3 & v3CurrentPos) const
{
	// Target Entity used for Pursue and Evade Behaviours
	Entity* TargetEntity = EntityTarget;
	// Target Entity Transform used for Pursue and Evade Behaviours
	Transform* TE_TransformComponent = static_cast<Transform*>(TargetEntity->FindComponentOfType(TRANSFORM));
	// Null check
	if (!TargetEntity)
	{
		return glm::vec3(0.0f, 0.0f, 0.0f);
	}
	// Extract the transform component
	TE_TransformComponent = static_cast<Transform*>(TargetEntity->FindComponentOfType(TRANSFORM));
	// Null check
	if (!TE_TransformComponent)
	{
		return glm::vec3(0.0f, 0.0f, 0.0f);
	}

	// Get the target's position
	glm::vec3 v3Target = TE_TransformComponent->GetCurrentPosition();
	// Set the target direction and velocity
	glm::vec3 v3TargetDirection = glm::normalize(v3CurrentPos - v3Target);
	glm::vec3 v3NewVelocity = v3TargetDirection * Max_Speed;
	// Calcukate the force
	glm::vec3 v3Force = v3NewVelocity - m_v3CurrentVelocity;
	// Display the gizmos
	Gizmos::addCircle(v3Target, 2.0f, 16, false, glm::vec4(0.f, 0.8f, 0.2f, 1.0f));
	Gizmos::addLine(v3Target, v3CurrentPos, glm::vec4(0.0f, 1.0f, 0.0f, 1.0f));
	// Return the force
	return v3Force;
}

// Calculate Pursue Force
glm::vec3 Brain::CalculatePursueForce(Entity* EntityTarget, const glm::vec3& v3CurrentPos) const
{
	// Target Entity used for Pursue and Evade Behaviours
	Entity* TargetEntity = EntityTarget;
	// Target Entity Transform used for Pursue and Evade Behaviours
	Transform* TE_TransformComponent = static_cast<Transform*>(TargetEntity->FindComponentOfType(TRANSFORM));
	// Null check
	if (!TargetEntity)
	{
		return glm::vec3(0.0f, 0.0f, 0.0f);
	}
	// Extract the transform component
	TE_TransformComponent = static_cast<Transform*>(TargetEntity->FindComponentOfType(TRANSFORM));
	// Null check
	if (!TE_TransformComponent)
	{
		return glm::vec3(0.0f, 0.0f, 0.0f);
	}
	// Get the target's position
	glm::vec3 v3Target = TE_TransformComponent->GetCurrentPosition();
	// Set the target direction and velocity
	glm::vec3 v3TargetDirection = glm::normalize(v3Target - v3CurrentPos);
	glm::vec3 v3NewVelocity = v3TargetDirection * Max_Speed;
	// Calcukate the force
	glm::vec3 v3Force = v3NewVelocity - m_v3CurrentVelocity;
	// Display the gizmos
	Gizmos::addCircle(v3Target, 2.0f, 16, false, glm::vec4(0.f, 0.8f, 0.2f, 1.0f));
	Gizmos::addLine(v3CurrentPos, v3Target, glm::vec4(1.0f, 0.0f, 0.0f, 1.0f));
	// Return the force
	return v3Force;
}

// Calculate Path Following Force
glm::vec3 Brain::CalculatePathFollowingForce(glm::vec3 PathArray[], const glm::vec3 & v3CurrentPos)
{
	// If the iterator is greater than the length of the path
	if (PathIterator > (unsigned)PathArray->length())
	{
		// Reset the iteator
		PathIterator = 0;
	}
	// Vec3 for the path follow force
	glm::vec3 PathFollowForce(0.0f, 0.0f, 0.0f);
	// Seek to the first point in the path
	PathFollowForce = CalculateSeekForce(PathArray[PathIterator], v3CurrentPos);
	// Calculate the distance between the object & point
	float Distance = glm::distance(v3CurrentPos, PathArray[PathIterator]);
	// If it's less than 1
	if (Distance < 1.f)
	{
		// Increment the iterator
		PathIterator = PathIterator + 1;
	}
	// Return the force
	return PathFollowForce;
}

// Calculate Containment Force
glm::vec3 Brain::CalculateContainmentForce(const glm::vec3 & v3CurrentPos)
{
	// Vec3 used for the containment force
	glm::vec3 ContainmentForce(0.0f, 0.0f, 0.0f);
	// If the current position is outside of the boundary
	if ((v3CurrentPos.x > 90) || (v3CurrentPos.x < -90) || (v3CurrentPos.z > 90)
		|| (v3CurrentPos.z < -90))
	{
		// Seek towards 0,0,0
		ContainmentForce = CalculateSeekForce(glm::vec3(0.0f, 0.0f, 0.0f), v3CurrentPos);
		// Set the containment timer to 5
		m_fContainmentTimer = 5.0f;
	}
	// Return the force
	return ContainmentForce;
}

// Calculate Arrival Force
glm::vec3 Brain::CalculateArrivalForce(const glm::vec3 & v3Target, const glm::vec3 & v3CurrentPos)
{
	// Vac3 used to hold the arrival force
	glm::vec3 ArrivalForce(0.0f, 0.0f, 0.0f);
	// Calculate the distance between the object & point
	float Distance = glm::distance(v3CurrentPos, v3Target);
	// If it's less than 10
	if (Distance < 10.f)
	{
		// Calculate a flee force which acts as a way to slow down the movement
		ArrivalForce = CalculateFleeForce(v3Target, v3CurrentPos);
		// Divide the force by 2
		ArrivalForce = ArrivalForce * 0.5f;
	}
	// Return the force
	return ArrivalForce;
}

// Calculate Obstacle Avoidance Force
glm::vec3 Brain::CalculateObstacleAvoidanceForce(const glm::vec3 & v3CurrentPos)
{
	// Vec3 used to store the Obstacle Avoidance force
	glm::vec3 v3ObstacleAvoidanceForce(0.0f, 0.0f, 0.0f);
	// If owner entity can be found
	if (GetOwnerEntity())
	{
		// Extract the transform component
		Transform* LocalTransformComponent = static_cast<Transform*>(GetOwnerEntity()->FindComponentOfType(TRANSFORM));
		// If it exists
		if (LocalTransformComponent)
		{
			// Vec3 to hold the local entity's position
			glm::vec3 v3LocalPos = LocalTransformComponent->GetCurrentPosition();
			// Get the obstacle map
			const std::map<const unsigned int, Entity*>& s_xObstacleMap = Entity::GetObstacleMap();
			// Iterator used to step through the map
			std::map<const unsigned int, Entity*>::const_iterator xIter_Obstacle;
			// For loop
			for (xIter_Obstacle = s_xObstacleMap.begin(); xIter_Obstacle != s_xObstacleMap.end(); xIter_Obstacle++)
			{
				// Set pTarget equal to the iterator's second value
				Entity* pTarget = xIter_Obstacle->second;
				// Null check
				if (!pTarget)
				{
					return glm::vec3(0.f, 0.f, 0.f);
				}
				// If the current entity and pTarget's entity ID are not equal
				if (pTarget->GetEntityID() != GetOwnerEntity()->GetEntityID())
				{
					// Extract the transform
					Transform* TargetTransformComponent = static_cast<Transform*>(pTarget->FindComponentOfType(TRANSFORM));
					// If extraction was successful
					if (TargetTransformComponent)
					{
						// Get the current position
						glm::vec3 v3TargetPos = TargetTransformComponent->GetCurrentPosition();
						// Calculate the distance between
						float fDistanceBetween = glm::length(v3LocalPos - v3TargetPos);
						// If the distance is less than 15
						if (fDistanceBetween < 15.0f)
						{
							// Add a flee force
							v3ObstacleAvoidanceForce += CalculateFleeForce(v3TargetPos, v3LocalPos);
							// If the force is > 0
							if (glm::length(v3ObstacleAvoidanceForce) > 0.0f)
							{
								// Normalize it
								glm::normalize(v3ObstacleAvoidanceForce);
							}
						}
					}
				}
			}
		}
	}
	// Return the force
	return v3ObstacleAvoidanceForce;
}

// Calculate Alignment Force
glm::vec3 Brain::CalculateAlignmentForce()
{
	// Vec3s used to store the average force & alignment force
	glm::vec3 v3AverageForce(0.0f, 0.0f, 0.0f);
	glm::vec3 v3AlignmentForce(0.0f, 0.0f, 0.0f);
	// Unsigned int used to count the amount of neighbours
	unsigned int uNeighborCount = 0;
	// If owner entity can be found
	if (GetOwnerEntity())
	{
		// Extract the transform component
		Transform* LocalTransformComponent = static_cast<Transform*>(GetOwnerEntity()->FindComponentOfType(TRANSFORM));
		// If it exists
		if (LocalTransformComponent)
		{
			// Vec3 to hold the local entity's position
			glm::vec3 v3LocalPos = LocalTransformComponent->GetCurrentPosition();
			// Get the boid map
			const std::map<const unsigned int, Entity*>& s_xBoidMap = Entity::GetBoidMap();
			// Iterator used to step through the map
			std::map<const unsigned int, Entity*>::const_iterator xIter;
			// For loop
			for (xIter = s_xBoidMap.begin(); xIter != s_xBoidMap.end(); xIter++)
			{
				// Set pTarget equal to the iterator's second value
				Entity* pTarget = xIter->second;
				// Null check
				if (!pTarget)
				{
					return glm::vec3(0.f, 0.f, 0.f);
				}
				// If the current entity and pTarget's entity ID are not equal
				if (pTarget->GetEntityID() != GetOwnerEntity()->GetEntityID())
				{
					// Extract the transform
					Transform* TargetTransformComponent = static_cast<Transform*>(pTarget->FindComponentOfType(TRANSFORM));
					// Extract the Brain
					Brain* TargetBrainComponent = static_cast<Brain*>(pTarget->FindComponentOfType(BRAIN));
					// If they exist
					if (TargetTransformComponent && TargetBrainComponent)
					{
						// Get the current position
						glm::vec3 v3TargetPos = TargetTransformComponent->GetCurrentPosition();
						// Calculate the distance between
						float fDistanceBetween = glm::length(v3LocalPos - v3TargetPos);
						// If the distance is less than 15
						if (fDistanceBetween < 15.0f)
						{
							// Add the target brain component's current velocity to the average
							v3AverageForce += TargetBrainComponent->GetCurrentVelocity();
							// Increment the neighbor count
							uNeighborCount = uNeighborCount + 1;
						}
					}
				}
			}
			// If the average force is > 0
			if (glm::length(v3AverageForce) > 0.0f)
			{
				// Divide the average force by the amount of neighbors to get the mean
				v3AverageForce /= uNeighborCount;
				// Calculate the alignment force
				v3AlignmentForce = glm::normalize(v3AverageForce - m_v3CurrentVelocity);
			}
		}
	}
	// Return the force
	return v3AlignmentForce;
}

// Calculate Seperation Force
glm::vec3 Brain::CalculateSeperationForce()
{
	// Vec3 used to store the seperation force
	glm::vec3 v3SeperationForce(0.0f, 0.0f, 0.0f);
	// If owner entity can be found
	if (GetOwnerEntity())
	{
		// Extract the transform component
		Transform* LocalTransformComponent = static_cast<Transform*>(GetOwnerEntity()->FindComponentOfType(TRANSFORM));
		// If it exists
		if (LocalTransformComponent)
		{
			// Vec3 to hold the local entity's position
			glm::vec3 v3LocalPos = LocalTransformComponent->GetCurrentPosition();
			// Get the Boid map
			const std::map<const unsigned int, Entity*>& s_xBoidMap = Entity::GetBoidMap();
			// Iterator used to step through the map
			std::map<const unsigned int, Entity*>::const_iterator xIter;
			// For loop
			for (xIter = s_xBoidMap.begin(); xIter != s_xBoidMap.end(); xIter++)
			{
				// Set pTarget equal to the iterator's second value
				Entity* pTarget = xIter->second;
				// Null check
				if (!pTarget)
				{
					return glm::vec3(0.f, 0.f, 0.f);
				}
				// If the current entity and pTarget's entity ID are not equal
				if (pTarget->GetEntityID() != GetOwnerEntity()->GetEntityID())
				{
					// Extract the transform component
					Transform* TargetTransformComponent = static_cast<Transform*>(pTarget->FindComponentOfType(TRANSFORM));
					// If extraction was successful
					if (TargetTransformComponent)
					{
						// Get the current position
						glm::vec3 v3TargetPos = TargetTransformComponent->GetCurrentPosition();
						// Calculate the distance between
						float fDistanceBetween = glm::length(v3LocalPos - v3TargetPos);
						// If the distance is less than 20
						if (fDistanceBetween < 20.0f)
						{
							// Add a flee force
							v3SeperationForce += CalculateFleeForce(v3TargetPos, v3LocalPos);
							// If the force is > 0
							if (glm::length(v3SeperationForce) > 0.0f)
							{
								// Normalize it
								glm::normalize(v3SeperationForce);
							}
						}
					}
				}
			}
		}
	}
	// Return the force
	return v3SeperationForce;
}

// Calculate Cohesion Force
glm::vec3 Brain::CalculateCohesionForce()
{
	// Vec3s used to store the average force & cohesion force
	glm::vec3 v3AveragePosition(0.0f, 0.0f, 0.0f);
	glm::vec3 v3CohesionForce(0.0f, 0.0f, 0.0f);
	// Unsigned int used to count the amount of neighbours
	unsigned int uNeighborCount = 0;
	// If owner entity can be found
	if (GetOwnerEntity())
	{
		// Extract the transform component
		Transform* LocalTransformComponent = static_cast<Transform*>(GetOwnerEntity()->FindComponentOfType(TRANSFORM));
		// If it exists
		if (LocalTransformComponent)
		{
			// Vec3 to hold the local entity's position
			glm::vec3 v3LocalPos = LocalTransformComponent->GetCurrentPosition();
			// Get the boid map
			const std::map<const unsigned int, Entity*>& s_xBoidMap = Entity::GetBoidMap();
			// Iterator used to step through the map
			std::map<const unsigned int, Entity*>::const_iterator xIter;
			// For loop
			for (xIter = s_xBoidMap.begin(); xIter != s_xBoidMap.end(); xIter++)
			{
				// Set pTarget equal to the iterator's second value
				Entity* pTarget = xIter->second;
				// Null check
				if (!pTarget)
				{
					return glm::vec3(0.f, 0.f, 0.f);
				}
				// If the current entity and pTarget's entity ID are not equal
				if (pTarget->GetEntityID() != GetOwnerEntity()->GetEntityID())
				{
					// Extract the transform
					Transform* TargetTransformComponent = static_cast<Transform*>(pTarget->FindComponentOfType(TRANSFORM));
					// If it exists
					if (TargetTransformComponent)
					{
						// Get the current position
						glm::vec3 v3TargetPos = TargetTransformComponent->GetCurrentPosition();
						// Calculate the distance between
						float fDistanceBetween = glm::length(v3LocalPos - v3TargetPos);
						// If the distance is less than 15
						if (fDistanceBetween < 15.0f)
						{
							// Add the target poaition to the average
							v3AveragePosition += v3TargetPos;
							// Increment the neighbor count
							uNeighborCount = uNeighborCount + 1;
						}
					}
				}
			}
			// If the average position is > 0
			if (glm::length(v3AveragePosition) > 0.0f)
			{
				// Divide the average position by the amount of neighbors to get the mean
				v3AveragePosition /= uNeighborCount;
				// Calculate the cohesion force
				v3CohesionForce = glm::normalize(v3AveragePosition - v3LocalPos);
			}
		}
	}
	// Return the force
	return v3CohesionForce;
}