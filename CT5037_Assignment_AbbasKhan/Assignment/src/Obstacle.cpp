///////////////////////////////////////////////////////////////////
// Name: Obstacle.cpp
// Author: Abbas Khan
// Bio: Entity Class to serve as the obstacles within the project
///////////////////////////////////////////////////////////////////

// Include the necessary header files
#include "Obstacle.h"
#include "Entity.h"
#include <GL/glew.h>
#include <glm/ext.hpp>
#include "Transform.h"
#include "Primitive.h"
#include "Collision.h"

// Typedef for Entity
typedef Entity Parent;

// Constructor
Obstacle::Obstacle()
{
	// Add a transform, primitive & collision component
	AddComponent(new Transform(this));
	AddComponent(new Primitive(this));
	AddComponent(new Collision(this));
}

// Destructor
Obstacle::~Obstacle()
{
}

// Update
void Obstacle::Update(float a_fDeltaTime)
{
	// Call the parent's update
	Parent::Update(a_fDeltaTime);
}

// Draw
void Obstacle::Draw(unsigned int a_uProgramID, unsigned int a_uVBO, unsigned int a_uIBO)
{
	// Call the parent's draw
	Parent::Draw(a_uProgramID, a_uVBO, a_uIBO);
}

// Set Position
void Obstacle::SetPosition(glm::vec3 TargetPosition)
{
	// Extract the transform component from the current object
	Transform* TransformComponent = static_cast<Transform*>(this->FindComponentOfType(TRANSFORM));
	// Null check
	if (!TransformComponent)
	{
		return;
	}
	// Set the transform component's current position
	TransformComponent->SetCurrentPosition(TargetPosition);
}
