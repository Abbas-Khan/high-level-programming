///////////////////////////////////////////////////////////////////
// Name: main.cpp
// Author: Abbas Khan
// Bio: Used as the main hub of the application
///////////////////////////////////////////////////////////////////

// Include the necessary header files
#include "Game.h"
#include "Constants.h"

// Main that controls the creation/destruction of an application
int main()
{
	// Explicitly control the creation of the application
	Game* game = new Game();
	// Call the run function through the game class
	game->run("CT5037 - Assignment 1", DEFAULT_SCREENWIDTH, DEFAULT_SCREENHEIGHT, false);
	// Xxplicitly control the destruction of the application
	delete game;
	// Return 0 to indicate the application ended correctly
	return 0;
}