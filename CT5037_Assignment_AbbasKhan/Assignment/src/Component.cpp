///////////////////////////////////////////////////////////////////
// Name: Component.cpp
// Author: Abbas Khan
// Bio: Used as the base class for all the components in the E.C.S
///////////////////////////////////////////////////////////////////

// Include the necessary header files
#include "Component.h"

// Constructor
Component::Component(Entity* a_OwnerEntity)
{
	// Set the owner entity
	m_pOwnerEntity = a_OwnerEntity;
}

// Destructor
Component::~Component()
{
}
