///////////////////////////////////////////////////////////////////
// Name: Game.cpp
// Author: Abbas Khan
// Bio: Manager class for the entire project
///////////////////////////////////////////////////////////////////

// This file's header
#include "Game.h"

// Core includes
#include <iostream>
#include <time.h>

// Framework includes
#include "Gizmos.h"
#include "Utilities.h"
#include "GL/glew.h"
#include "glfw3.h"
#include "glm/ext.hpp"

// External Library Includes
#include "imgui.h"
#include "fmod.hpp"

// Project includes
#include "Boid.h"
#include "Transform.h"
#include "Brain.h"
#include "Pinky.h"
#include "Obstacle.h"
#include "Collision.h"

// SFX FMOD Variables
FMOD::System* m_FMOD_System;
FMOD::Sound* m_FMOD_Sound;
FMOD::Channel* m_FMOD_Channel = 0;
bool b_FMOD_Played = false;

// BGM FMOD Variables
FMOD::System* m_FMOD_System_BGM;
FMOD::Sound* m_FMOD_Sound_BGM;
FMOD::Channel* m_FMOD_Channel_BGM = 0;
bool b_FMOD_Played_BGM = false;
float fTimer = 5;

// Static variable local definitions
Game* Game::xGameInstance;

// GLFW Mouse Button Click Callback function 
void mouseButtonCallback(GLFWwindow *window, int button, int action, int mods)
{
	// On left mouse press
	if (button == GLFW_MOUSE_BUTTON_LEFT && action == GLFW_PRESS)
	{
		// Check if sound is already playing
		m_FMOD_Channel->isPlaying(&b_FMOD_Played);
		// If not
		if (b_FMOD_Played == false)
		{
			// Play the sound
			m_FMOD_System->playSound(m_FMOD_Sound, 0, false, &m_FMOD_Channel);
		}
	}
}

// Constructor
Game::Game()
{
	xGameInstance = this;
	#pragma region Physics Init
	// Bullet Physics initial setup
	m_pCollisionConfig = new btDefaultCollisionConfiguration();
	m_pDispatcher = new btCollisionDispatcher(m_pCollisionConfig);
	m_pBroadPhase = new btDbvtBroadphase();
	m_pSolver = new btSequentialImpulseConstraintSolver();
	m_pWorld = new btDiscreteDynamicsWorld(m_pDispatcher, m_pBroadPhase, m_pSolver, m_pCollisionConfig);
	if (m_pWorld)
	{
		m_pWorld->setGravity(btVector3(0.0f, -10.0f, 0.0f));
		btContactSolverInfo& info = m_pWorld->getSolverInfo();
		info.m_splitImpulse = 1;
		info.m_splitImpulsePenetrationThreshold = -0.02f;
	}
	#pragma endregion
}

// Destructor
Game::~Game()
{
}

// Static function to retrieve the game instance
Game& Game::GetGame()
{
	return *xGameInstance;
}

// OnCreate boolean function
bool Game::onCreate()
{
	// Initialise the Gizmos helper class
	Gizmos::create();

	#pragma region SFX Button FMOD Setup
	// Setup the Button SFX FMOD Variables
	FMOD::System_Create(&m_FMOD_System);
	m_FMOD_System->init(32, FMOD_INIT_NORMAL, nullptr);
	m_FMOD_System->createSound("../resources/Audio/RobotBlip.wav", FMOD_DEFAULT, 0, &m_FMOD_Sound);
	m_FMOD_Sound->setMode(FMOD_LOOP_OFF);
	#pragma endregion

	#pragma region BGM FMOD Setup
	// Setup the BGM FMOD Variables
	FMOD::System_Create(&m_FMOD_System_BGM);
	m_FMOD_System_BGM->init(32, FMOD_INIT_NORMAL, nullptr);
	m_FMOD_System_BGM->createSound("../resources/Audio/Babylon.mp3", FMOD_DEFAULT, 0, &m_FMOD_Sound_BGM);
	m_FMOD_Sound_BGM->setMode(FMOD_LOOP_NORMAL);
	#pragma endregion

	#pragma region World Setup
	// Set the position of the light
	m_lightPos = glm::vec4(0.f, 50.f, 0.f, 1.f);

	// Load the shaders for this program
	m_vertexShader = Utility::loadShader("../resources/shaders/vertex.glsl", GL_VERTEX_SHADER);
	m_fragmentShader = Utility::loadShader("../resources/shaders/fragment.glsl", GL_FRAGMENT_SHADER);

	// Define the input and output varialbes in the shaders
	// Note: these names are taken from the glsl files -- added in inputs for UV coordinates
	const char* szInputs[] = { "Position", "Normal", "Tangent", "Indices", "Weights","Tex1" };
	const char* szOutputs[] = { "FragColor" };

	// bind the shaders to create our shader program
	m_programID = Utility::createProgram(
		m_vertexShader,
		0,
		0,
		0,
		m_fragmentShader,
		6, szInputs, 1, szOutputs);

	// Generate our OpenGL Vertex and Index Buffers for rendering our FBX Model Data
	// OPENGL: generate the VBO, IBO and VAO
	glGenBuffers(1, &m_vbo);
	glGenBuffers(1, &m_ibo);
	glGenVertexArrays(1, &m_vao);

	// OPENGL: Bind  VAO, and then bind the VBO and IBO to the VAO
	glBindVertexArray(m_vao);
	glBindBuffer(GL_ARRAY_BUFFER, m_vbo);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_ibo);

	// There is no need to populate the vbo & ibo buffers with any data at this stage
	// this can be done when rendering each mesh component of the FBX model

	// enable the attribute locations that will be used on our shaders
	glEnableVertexAttribArray(0); //Pos
	glEnableVertexAttribArray(1); //Norm
	glEnableVertexAttribArray(2); //tangent
	glEnableVertexAttribArray(3); //Indices
	glEnableVertexAttribArray(4); //Weights
	glEnableVertexAttribArray(5); //Tex1

	// tell our shaders where the information within our buffers lie
	glVertexAttribPointer(0, 4, GL_FLOAT, GL_FALSE, sizeof(MD5Vertex), ((char *)0) + MD5Vertex::PositionOffset);
	glVertexAttribPointer(1, 4, GL_FLOAT, GL_TRUE, sizeof(MD5Vertex), ((char *)0) + MD5Vertex::NormalOffset);
	glVertexAttribPointer(2, 4, GL_FLOAT, GL_FALSE, sizeof(MD5Vertex), ((char *)0) + MD5Vertex::TangentOffset);
	glVertexAttribPointer(3, 4, GL_FLOAT, GL_FALSE, sizeof(MD5Vertex), ((char *)0) + MD5Vertex::IndicesOffset);
	glVertexAttribPointer(4, 4, GL_FLOAT, GL_TRUE, sizeof(MD5Vertex), ((char *)0) + MD5Vertex::WeightsOffset);
	glVertexAttribPointer(5, 2, GL_FLOAT, GL_TRUE, sizeof(MD5Vertex), ((char *)0) + MD5Vertex::TexCoord1Offset);

	// finally, where done describing our mesh to the shader
	// we can describe the next mesh
	glBindVertexArray(0);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

	// create a world-space matrix for a camera
	m_cameraMatrix = glm::inverse(glm::lookAt(glm::vec3(50, 50, 50), glm::vec3(0, 0, 0), glm::vec3(0, 1, 0)));

	// create a perspective projection matrix with a 90 degree field-of-view and widescreen aspect ratio
	m_projectionMatrix = glm::perspective(glm::pi<float>() * 0.25f, DEFAULT_SCREENWIDTH / (float)DEFAULT_SCREENHEIGHT, 0.1f, 1000.0f);

	// set the clear colour and enable depth testing and backface culling
	glClearColor(0.25f, 0.25f, 0.25f, 1.f);
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_CULL_FACE);
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	#pragma endregion

	// Call create boids
	CreateBoids();

	#pragma region Physics Setup
	// Create a world floor
	btCollisionShape* groundShape = new btStaticPlaneShape(btVector3(0, 1, 0), 1);
	btDefaultMotionState* groundMotionState = new btDefaultMotionState(btTransform(btQuaternion(0, 0, 0, 1), btVector3(0, 5, 0)));
	btRigidBody::btRigidBodyConstructionInfo groundRigidBodyCI(0, groundMotionState, groundShape, btVector3(0, 0, 0));
	btRigidBody* groundRigidBody = new btRigidBody(groundRigidBodyCI);
	m_pWorld->addRigidBody(groundRigidBody);

	// Small test to ensure all objects have been added to dynamic world (Will equal amount of enemies on start + obstacles)
	std::cout << "Amount of objects loaded into Physics: " << m_pWorld->getNumCollisionObjects() << "\n";
	#pragma endregion

	return true;
}

// Update function
void Game::Update(float a_deltaTime)
{

	#pragma region IMGUI
	// Create the IMGUI Window & add the various segments
	ImGui::Begin("Settings");
	ImGui::Separator();
	// Information section
	ImGui::TextColored(ImVec4(0, 1, 0.8f, 1), "Information");
	ImGui::Separator();
	ImGui::TextWrapped("Welcome to Abbas Khan's boid simulation for CT5037 - High Level Programming");
	ImGui::Separator();
	ImGui::TextWrapped("Below you will find two seperate sections which can be modified. The first is to do with obstacle creation & the second is to do with the steering behaviours for the boids.");
	ImGui::TextWrapped("It is important to note the following points:");
	ImGui::BulletText("The boids must have some form of wander force in order to move");
	ImGui::BulletText("Objects locations cannot be outside of the 75 range");
	ImGui::Separator();
	// Box Generation section
	ImGui::TextColored(ImVec4(0, 1, 0.8f, 1), "Box Generation");
	ImGui::Separator();
	// Sliders used for positioning
	static int i_x = 0;
	ImGui::SliderInt("X Position", &i_x, -75, 75);
	static int i_z = 0;
	ImGui::SliderInt("Z Position", &i_z, -75, 75);
	// Buttons used for creation
	if (ImGui::Button("Create Box At Location"))
	{
		CreateObstacle(i_x, i_z);
	}
	if (ImGui::Button("Create Random Box"))
	{
		CreateObstacleRandom();
	}
	ImGui::Separator();
	// Steering Behaviour section
	ImGui::TextColored(ImVec4(0, 1, 0.8f, 1), "Steering Behaviour");
	ImGui::Separator();
	// Sliders used for altering behaviour
	static float f_Alignment = 0.0f;
	ImGui::SliderFloat("Alignment", &f_Alignment, 0.0f, 1.0f);
	static float f_Cohesion = 0.0f;
	ImGui::SliderFloat("Cohesion", &f_Cohesion, 0.0f, 1.0f);
	static float f_Seperation = 0.7f;
	ImGui::SliderFloat("Seperation", &f_Seperation, 0.0f, 1.0f);
	ImGui::Separator();
	static float f_Wander = 0.3f;
	ImGui::SliderFloat("Wander", &f_Wander, 0.0f, 1.0f);
	static float f_ObstacleAvoidance = 0.0f;
	ImGui::SliderFloat("Obstacle Avoidance", &f_ObstacleAvoidance, 0.0f, 1.0f);
	ImGui::Separator();
	static float f_Speed = 5.5f;
	ImGui::SliderFloat("Max Speed", &f_Speed, 1.0f, 6.0f);
	static float f_Jitter = 0.1f;
	ImGui::SliderFloat("Jitter", &f_Jitter, 0.0f, 1.0f);
	static float f_WanderRadius = 2.0f;
	ImGui::SliderFloat("Wander Radius", &f_WanderRadius, 0.0f, 3.0f);
	static float f_CircleForwardMultiplier = 5.0f;
	ImGui::SliderFloat("Circle Forward Multiplier", &f_CircleForwardMultiplier, 0.0f, 6.0f);
	ImGui::Separator();
	// Credits section
	ImGui::TextColored(ImVec4(0, 1, 0.8f, 1), "Credits");
	ImGui::Separator();
	ImGui::TextWrapped("The following Audio files were used within this assignment.");
	ImGui::TextWrapped("Robot Blip Sound - Marianne Gagnon (SoundBible.com)");
	ImGui::TextWrapped("Lion Growling Sounds - Mike Koenig (SoundBible.com)");
	ImGui::TextWrapped("Tiger Growling Sounds - Mike Koenig (SoundBible.com)");
	ImGui::TextWrapped("Babylon - Kevin MacLeod (incompetech.com)");
	ImGui::Separator();
	ImGui::TextWrapped("All of these sounds are licensed under CC 3.0 http://creativecommons.org/licenses/by/3.0/");
	ImGui::End();
	// Get the boid map
	const std::map<const unsigned int, Entity*>& s_xBoidMap = Entity::GetBoidMap();
	// Iterator used to step through the map
	std::map<const unsigned int, Entity*>::const_iterator xIter;
	// For loop
	for (xIter = s_xBoidMap.begin(); xIter != s_xBoidMap.end(); ++xIter)
	{
		// Set the current entity to the iterators value
		Entity* pCurrentEntity = xIter->second;
		// Extract the brain component
		Brain* BrainComponent = static_cast<Brain*>(pCurrentEntity->FindComponentOfType(BRAIN));
		// Null check
		if (!BrainComponent)
		{
			return;
		}
		// Update the boids brain component multipliers
		BrainComponent->Alignment_Multiplier = f_Alignment;
		BrainComponent->Cohesion_Multiplier = f_Cohesion;
		BrainComponent->Seperation_Multiplier = f_Seperation;
		BrainComponent->Wander_Multiplier = f_Wander;
		BrainComponent->ObstacleAvoidance_Multiplier = f_ObstacleAvoidance;
		BrainComponent->Max_Speed = f_Speed;
		BrainComponent->Jitter = f_Jitter;
		BrainComponent->Wander_Radius = f_WanderRadius;
		BrainComponent->Circle_Forward_Multiplier = f_CircleForwardMultiplier;
	}
	#pragma endregion

	// Fix for NaN issues in the model matrix
	if (a_deltaTime <= 0.f) return;

	#pragma region Bullet
	// Step through the simulation
	m_pWorld->stepSimulation(1 / 60.f, 10);
	#pragma endregion

	#pragma region BGM Fmod PLay
	// If the background music is not played
	if (b_FMOD_Played_BGM == false)
	{
		// Play it
		m_FMOD_System_BGM->playSound(m_FMOD_Sound_BGM, 0, false, &m_FMOD_Channel_BGM);
		// Set played to true
		b_FMOD_Played_BGM = true;
	}
	#pragma endregion

	#pragma region SFX Button Fmod PLay
	// Call the callback function
	glfwSetMouseButtonCallback(m_window, mouseButtonCallback);
	#pragma endregion

	#pragma region World Setup
	// update the camera matrix using the keyboard/mouse
	Utility::freeMovement(m_cameraMatrix, a_deltaTime, 30);

	// clear all gizmos from last frame
	Gizmos::clear();

	// Create the world floor
	Gizmos::addBox(glm::vec3(0, 6, 0), glm::vec3(100.0f, 0.f, 100.0f), true, glm::vec4(0.f, 0.0f, 0.f, 1.f));

	// Create the world grid
	const int iMaxGridSize = 200;
	const int iHalfGridSize = iMaxGridSize / 2;
	const int iGridDensity = 4;
	for (int i = 0; i <= 200; i += iGridDensity)
	{
		Gizmos::addLine(glm::vec3(-iHalfGridSize + i, 6, iHalfGridSize),
			glm::vec3(-iHalfGridSize + i, 6, -iHalfGridSize),
			i == iHalfGridSize ? glm::vec4(1, 1, 1, 1) : glm::vec4(0, 0, 1, 1));

		Gizmos::addLine(glm::vec3(iHalfGridSize, 6, -iHalfGridSize + i),
			glm::vec3(-iHalfGridSize, 6, -iHalfGridSize + i),
			i == iHalfGridSize ? glm::vec4(1, 1, 1, 1) : glm::vec4(0, 0, 1, 1));
	}

	// Create box to represent the light
	Gizmos::addBox(m_lightPos, glm::vec3(0.2f, 0.2f, 0.2f), true, glm::vec4(1.0f, 0.85f, 0.05f, 1.f));
	#pragma endregion

	#pragma region Update Entities
	// Get the boid map
	const std::map<const unsigned int, Entity*>& s_xBoidMapLocal = Entity::GetBoidMap();
	// Iterator used to step through the map
	std::map<const unsigned int, Entity*>::const_iterator xIter_Boid;
	// For loop
	for (xIter_Boid = s_xBoidMapLocal.begin(); xIter_Boid != s_xBoidMapLocal.end(); ++xIter_Boid)
	{
		// Set the current entity to the iterators value
		Entity* pCurrentEntity = xIter_Boid->second;
		// If it exists
		if (pCurrentEntity)
		{
			// Call update
			pCurrentEntity->Update(a_deltaTime);
		}
	}
	// Get the Obstacle map
	const std::map<const unsigned int, Entity*>& s_xObstacleMap = Entity::GetObstacleMap();
	// Iterator used to step through the map
	std::map<const unsigned int, Entity*>::const_iterator xIter_Obstacle;
	// For loop
	for (xIter_Obstacle = s_xObstacleMap.begin(); xIter_Obstacle != s_xObstacleMap.end(); ++xIter_Obstacle)
	{
		// Set the current entity to the iterators value
		Entity* pCurrentEntity = xIter_Obstacle->second;
		// If it exists
		if (pCurrentEntity)
		{
			// Call Update
			pCurrentEntity->Update(a_deltaTime);
		}
	}
	#pragma endregion

	// quit our application when escape is pressed
	if (glfwGetKey(m_window, GLFW_KEY_ESCAPE) == GLFW_PRESS)
		quit();
}

// Draw function
void Game::Draw()
{
	// clear the backbuffer
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	// get the view matrix from the world-space camera matrix
	glm::mat4 viewMatrix = glm::inverse(m_cameraMatrix);

	// bind shader program
	glUseProgram(m_programID);
	// bind vertex array object
	glBindVertexArray(m_vao);

	// get shaders uniform location for our projectionViewMatrix and then use glUniformMatrix4fv to fill it with the correct data
	unsigned int projectionViewUniform = glGetUniformLocation(m_programID, "ProjectionView");
	glUniformMatrix4fv(projectionViewUniform, 1, false, glm::value_ptr(m_projectionMatrix * viewMatrix));

	// pass camera position to fragment shader uniform
	unsigned int cameraPosUniform = glGetUniformLocation(m_programID, "cameraPosition");
	glUniform4fv(cameraPosUniform, 1, glm::value_ptr(m_cameraMatrix[3]));

	// pass the directional light direction to fragment shader
	glm::vec4 lightDir = -m_lightPos;
	lightDir.w = 0.f;
	lightDir = glm::normalize(lightDir);
	unsigned int lightDirUniform = glGetUniformLocation(m_programID, "lightDirection");
	glUniform4fv(lightDirUniform, 1, glm::value_ptr(lightDir));

	// Get the boid map
	const std::map<const unsigned int, Entity*>& s_xBoidMap = Entity::GetBoidMap();
	// Iterator used to step through the map
	std::map<const unsigned int, Entity*>::const_iterator xIter_Boid;
	// For loop
	for (xIter_Boid = s_xBoidMap.begin(); xIter_Boid != s_xBoidMap.end(); ++xIter_Boid)
	{
		// Set the current entity to the iterators value
		Entity* pCurrentEntity = xIter_Boid->second;
		// If it exists
		if (pCurrentEntity)
		{
			// Call draw
			pCurrentEntity->Draw(m_programID, m_vbo, m_ibo);
		}
	}

	// Get the obstacle map
	const std::map<const unsigned int, Entity*>& s_xObstacleMap = Entity::GetObstacleMap();
	// Iterator used to step through the map
	std::map<const unsigned int, Entity*>::const_iterator xIter_Obstacle;
	// For loop
	for (xIter_Obstacle = s_xObstacleMap.begin(); xIter_Obstacle != s_xObstacleMap.end(); ++xIter_Obstacle)
	{
		// Set the current entity to the iterators value
		Entity* pCurrentEntity = xIter_Obstacle->second;
		// If it exists
		if (pCurrentEntity)
		{
			// Call draw
			pCurrentEntity->Draw(m_programID, m_vbo, m_ibo);
		}
	}

	glBindVertexArray(0);
	glUseProgram(0);

	// draw the gizmos from this frame
	Gizmos::draw(viewMatrix, m_projectionMatrix);
}

// Destroy function
void Game::Destroy()
{
	Gizmos::destroy();
}

// Create boids function
void Game::CreateBoids()
{
	// Seed the random
	srand((unsigned int)time(nullptr));

	// Create all our boids
	for (int i = 0; i < iMAX_NUM_OF_BOIDS; ++i)
	{
		// Create a new boid
		Boid* pNewBoid = new Pinky();
		
		// Generate a random position
		float x = (float)(rand() % 151 + (-50));
		float z = (float)(rand() % 151 + (-50));

		// Extract the transform
		Transform* TransformComponent = static_cast<Transform*>(pNewBoid->FindComponentOfType(TRANSFORM));

		// Null check
		if (!TransformComponent)
		{
			return;
		}

		// Set the current position
		TransformComponent->SetCurrentPosition(glm::vec3(x, 15.f, z));

		// Random facing direction
		glm::vec3 forward(glm::vec3(x, 0.f, z));
		forward = glm::normalize(forward);
		glm::vec3 up = TransformComponent->GetUpDirection();
		glm::vec3 right = glm::cross(up, forward);

		// Set directions
		TransformComponent->SetRightDirection(right);
		TransformComponent->SetFacingDirection(forward);

		// Add to map
		Entity* pEntityTarget = static_cast<Entity*>(pNewBoid);
		if (!pEntityTarget)
		{
			return;
		}
		pEntityTarget->AddToMap(i, pEntityTarget, true);
	}
}

// Create obstacle random function
void Game::CreateObstacleRandom()
{
	// Seed the random
	srand((unsigned int)time(nullptr));

	// Create a new obstacle
	Entity* pNewObstacle = new Obstacle();

	// Random start position
	float x = (float)(rand() % 101 + (-50));
	float z = (float)(rand() % 101 + (-50));

	// Extract the transform
	Transform* TransformComponent = static_cast<Transform*>(pNewObstacle->FindComponentOfType(TRANSFORM));

	// Null check
	if (!TransformComponent)
	{
		return;
	}

	// Set the current position
	TransformComponent->SetCurrentPosition(glm::vec3(x, 15.f, z));

	// Random facing direction
	glm::vec3 forward(glm::vec3(x, 0.f, z));
	forward = glm::normalize(forward);
	glm::vec3 up = TransformComponent->GetUpDirection();
	glm::vec3 right = glm::cross(up, forward);

	// Set directions
	TransformComponent->SetRightDirection(right);
	TransformComponent->SetFacingDirection(forward);

	// Add to map
	Entity* pEntityTarget = static_cast<Entity*>(pNewObstacle);
	if (!pEntityTarget)
	{
		return;
	}
	const std::map<const unsigned int, Entity*>& s_xEntityMap = Entity::GetEntityMap();
	pEntityTarget->AddToMap((unsigned int)s_xEntityMap.size() + 1, pEntityTarget, false);
}

// Create obstacle function
void Game::CreateObstacle(int a_x, int a_z)
{
	// Seed the random
	srand((unsigned int)time(nullptr));

	// Create a new obstacle
	Entity* pNewObstacle = new Obstacle();

	// Random start position
	float x = (float)a_x;
	float z = (float)a_z;

	// Extract the transform
	Transform* TransformComponent = static_cast<Transform*>(pNewObstacle->FindComponentOfType(TRANSFORM));

	// Null check
	if (!TransformComponent)
	{
		return;
	}

	// Set the current position
	TransformComponent->SetCurrentPosition(glm::vec3(x, 15.f, z));

	// Random facing direction
	glm::vec3 forward(glm::vec3(x, 0.f, z));
	forward = glm::normalize(forward);
	glm::vec3 up = TransformComponent->GetUpDirection();
	glm::vec3 right = glm::cross(up, forward);

	// Set directions
	TransformComponent->SetRightDirection(right);
	TransformComponent->SetFacingDirection(forward);

	// Add to map
	Entity* pEntityTarget = static_cast<Entity*>(pNewObstacle);
	if (!pEntityTarget)
	{
		return;
	}
	const std::map<const unsigned int, Entity*>& s_xEntityMap = Entity::GetEntityMap();
	pEntityTarget->AddToMap((unsigned int)s_xEntityMap.size() + 1, pEntityTarget, false);
}