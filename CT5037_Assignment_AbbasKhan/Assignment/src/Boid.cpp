///////////////////////////////////////////////////////////////////
// Name: Boid.cpp
// Author: Abbas Khan
// Bio: Entity Class to serve as the pinky Parent
///////////////////////////////////////////////////////////////////

// Include the necessary header files
#include "Boid.h"
#include "Entity.h"
#include <GL/glew.h>
#include <glm/ext.hpp>
#include "Transform.h"
#include "Brain.h"
#include "Model.h"
#include "Audio.h"
#include "Collision.h"

// Typedef for Entity
typedef Entity Parent;

// Constructor
Boid::Boid()
	: m_fScale(0.2f)
{
	// Add a transform, model, brain, audio & collision component
	AddComponent(new Transform(this));
	AddComponent(new Brain(this));
	AddComponent(new Model(this));
	AddComponent(new Audio(this));
	AddComponent(new Collision(this));
}

// Destructor
Boid::~Boid()
{
}

// Update
void Boid::Update(float a_fDeltaTime)
{
	// Call the parent's update
	Parent::Update(a_fDeltaTime);
}

// Draw
void Boid::Draw(unsigned int a_uProgramID, unsigned int a_uVBO, unsigned int a_uIBO)
{
	// Call the parent's draw
	Parent::Draw(a_uProgramID, a_uVBO, a_uIBO);
}