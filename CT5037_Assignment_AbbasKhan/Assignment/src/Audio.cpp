///////////////////////////////////////////////////////////////////
// Name: Audio.cpp
// Author: Abbas Khan
// Bio: Component Class used to control the Audio
///////////////////////////////////////////////////////////////////

// Include the necessary header files
#include "Audio.h"

// Typedef component as parent
typedef Component Parent;

// Constructor
Audio::Audio(Entity * a_OwnerEntity)
	: Parent(a_OwnerEntity)
{
	// Set the component tyoe
	m_eComponentType = AUDIO;

	// Set up FMOD
	FMOD::System_Create(&m_FMOD_System);
	m_FMOD_System->init(32, FMOD_INIT_NORMAL, nullptr);

	// Randomly select a sound effect
	int Rand = (rand() > RAND_MAX / 2) ? 0 : 1;
	char* AudioPath = "";
	if (Rand == 0)
	{
		AudioPath = "../resources/Audio/TigerGrowl.wav";
	}
	else
	{
		AudioPath = "../resources/Audio/LionGrowl.wav";
	}

	// Set the initial timer
	InitialfTimer = (float)(rand() % 10) + 15.0f;

	// Create the sound & set the mode
	Audio::CreateSound(AudioPath, FMOD_DEFAULT, 0, &m_FMOD_Sound);
	Audio::SetMode(FMOD_LOOP_OFF);
}

// Destructor
Audio::~Audio()
{
}

// Update
void Audio::Update(float a_fDeltaTime)
{
	// Get whether the sound is currently playing
	m_FMOD_Channel->isPlaying(&b_FMOD_Played);
	// Decrement the timer
	fTimer = fTimer - a_fDeltaTime;
	// If the sound is not playing and the timer is <= 0
	if (b_FMOD_Played == false && fTimer <= 0)
	{
		// Play the sound
		Audio::PlaySound(m_FMOD_Sound, 0, false, &m_FMOD_Channel);
		m_FMOD_Channel->setVolume(0.2f);
		// Reset the timer
		fTimer = InitialfTimer;
	}
}

// Create sound function
void Audio::CreateSound(const char * name_or_data, FMOD_MODE mode, FMOD_CREATESOUNDEXINFO * exinfo, FMOD::Sound ** sound)
{
	// Create the sound
	m_FMOD_System->createSound(name_or_data, mode, exinfo, sound);
}

// Set mode function
void Audio::SetMode(FMOD_MODE mode)
{
	// Set the mode
	m_FMOD_Sound->setMode(mode);
}

// Play sound function
void Audio::PlaySound(FMOD::Sound * sound, FMOD::ChannelGroup * channelgroup, bool paused, FMOD::Channel ** channel)
{
	// Play the sound
	m_FMOD_System->playSound(sound, channelgroup, paused, channel);
}
