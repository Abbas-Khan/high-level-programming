///////////////////////////////////////////////////////////////////
// Name: Collision.cpp
// Author: Abbas Khan
// Bio: Component Class used to control the behaviour of the AI
///////////////////////////////////////////////////////////////////

// Include the necessary header files
#include "Collision.h"
#include "Gizmos.h"
#include "Transform.h"
#include "Brain.h"
#include "Game.h"

// Typedef component as parent
typedef Component Parent;

// Constructor
Collision::Collision(Entity * A_pOwner)
	: Parent(A_pOwner)
{
	// Set the component type
	m_eComponentType = COLLISION;
	// Call create rigidbody
	CreateRigidBody();
}

// Destructor
Collision::~Collision()
{
}

// Update
void Collision::Update(float a_fDeltaTime)
{
	// If the timer is greater than 0.f
	if (Ftimer > 0.f)
	{
		// Decrement it
		Ftimer = Ftimer - a_fDeltaTime;
	}
	// Otherwise
	else if (Ftimer <= 0.f)
	{
		// Can update is true
		CanUpdate = true;
	}
	// If can update is true
	if (CanUpdate == true)
	{
		// Floats for the x, y & z position of the entity
		float fX = 0.0f, fY = 0.0f, fZ = 0.0f;
		// Get the owner entity
		Entity* pOwner = GetOwnerEntity();
		// If it exists
		if (pOwner)
		{
			// If a rigidbody exists
			if (m_pRigidBody)
			{
				// Bullet transform to hold the rigidbody transform data
				btTransform RigidBodyTransform;
				// Set it to identity
				RigidBodyTransform.setIdentity();
				// Get the rigidbody's position
				RigidBodyTransform = m_pRigidBody->getWorldTransform();
				// Extract the position
				btVector3 TenpVec = RigidBodyTransform.getOrigin();
				// Get the position values
				fX = TenpVec.getX();
				fY = TenpVec.getY();
				fZ = TenpVec.getZ();
				// fY Safe Gaurd as occasionally object's can jump
				if (fY > 15.0f)
				{
					fY = 11.0f;
				}
				// fY Safe Gaurd as occasionally object's can fall
				if (fY < 10.9f)
				{
					fY = 11.0f;
				}
			}
			// Extract the transform component
			Transform* TransformComponent = static_cast<Transform*>(pOwner->FindComponentOfType(TRANSFORM));
			// If it exists
			if (TransformComponent)
			{
				// Get the position values (fY is determined by the RigidBody as Gravity controls it)
				fX = TransformComponent->GetCurrentPosition().x;
				fZ = TransformComponent->GetCurrentPosition().z;
				// Set the position values
				TransformComponent->SetCurrentPosition(glm::vec3(fX, fY, fZ));
			}
			// Create a bullet transform
			btTransform t;
			// Set it to identity
			t.setIdentity();
			// Set the origin to the transform component
			t.setOrigin(btVector3(fX, fY, fZ));
			// Set the rigid body's position
			m_pRigidBody->setWorldTransform(t);
			// Draw a box at the transform
			Gizmos::addBox(glm::vec3(fX, fY, fZ), glm::vec3(5.0f, 5.0f, 5.0f), false);
		}
	}
}

// Create rigid body function
void Collision::CreateRigidBody()
{
	// Floats for the x, y & z position of the entity
	float fX = 0.0f, fY = 0.0f, fZ = 0.0f;
	// Get the owner entity
	Entity* pOwner = GetOwnerEntity();
	// If it exists
	if (pOwner)
	{
		// Extract the transform component
		Transform* TransformComponent = static_cast<Transform*>(pOwner->FindComponentOfType(TRANSFORM));
		// If it exists
		if (TransformComponent)
		{
			// Get the position values
			fX = TransformComponent->GetCurrentPosition().x;
			fY = TransformComponent->GetCurrentPosition().y;
			fZ = TransformComponent->GetCurrentPosition().z;
		}
	}
	// Create a bullet box
	btBoxShape* pBox = new btBoxShape(btVector3(5.0f, 5.0f, 5.0f));

	// Calculate the inertia
	float fMass = 10.0f;
	btVector3 v3Inertia(0.0f, 0.0f, 0.0f);
	pBox->calculateLocalInertia(fMass, v3Inertia);

	// Create a bullet transform
	btTransform t;
	// Set it to identity
	t.setIdentity();
	// Set the origin to the transform component
	t.setOrigin(btVector3(fX, fY, fZ));
	// Set the default motion state
	btMotionState* pMotion = new btDefaultMotionState(t);
	// Set the construction info
	btRigidBody::btRigidBodyConstructionInfo pInfo(fMass, pMotion, pBox, v3Inertia);
	// Create the rigidbody
	m_pRigidBody = new btRigidBody(pInfo);
	// If a rigidbody exists
	if (m_pRigidBody)
	{
		// Set the rigidbody's position
		m_pRigidBody->setWorldTransform(t);
	}
	// Get the game instance
	Game& xGame = Game::GetGame();
	// Get the dynamic world
	btDynamicsWorld* pWorld = xGame.GetDynamicWorld();
	// If it exists
	if (pWorld)
	{
		// Add the rigidbody
		pWorld->addRigidBody(m_pRigidBody);
	}
}