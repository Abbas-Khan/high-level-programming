///////////////////////////////////////////////////////////////////
// Name: Pinky.cpp
// Author: Abbas Khan
// Bio: Boid Class to serve as the boids within the project
///////////////////////////////////////////////////////////////////

// Include the necessary header files
#include "Pinky.h"

// Typedef for Boid
typedef Boid Parent;

// Constructor
Pinky::Pinky()
{
	// Call load model on each object creation
	LoadModel();
}

// Destructor
Pinky::~Pinky()
{
}

// Update
void Pinky::Update(float a_fDeltaTime)
{
	// Call parent's update
	Parent::Update(a_fDeltaTime);
}

// Draw
void Pinky::Draw(unsigned int a_uProgramID, unsigned int a_uVBO, unsigned int a_uIBO)
{
	// Call parent's draw
	Parent::Draw(a_uProgramID, a_uVBO, a_uIBO);
}

// Load Model
void Pinky::LoadModel()
{
	// Extract the model component
	Model* ModelComp = static_cast<Model*>(this->FindComponentOfType(MODEL));
	// Null Check
	if (!ModelComp)
	{
		return;
	}
	// Pinky file paths
	const unsigned int g_uNumOfPinkyAnims = 2;
	const char* aPinkyAnims[g_uNumOfPinkyAnims] =
	{
		"../resources/Animations/pinky/idle1.md5anim",
		"../resources/Animations/pinky/run.md5anim"
	};
	// Load pinky
	ModelComp->LoadModel("../resources/Models/pinky.md5mesh", 0.1f);
	ModelComp->LoadAnim(g_uNumOfPinkyAnims, aPinkyAnims);
	// Set the default animation
	ModelComp->SetCurrentAnim("idle1");
}