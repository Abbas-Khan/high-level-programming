///////////////////////////////////////////////////////////////////
// Name: Transform.cpp
// Author: Abbas Khan
// Bio: Component Class used to control the transform of the AI
///////////////////////////////////////////////////////////////////

// Include the necessary header files
#include "Transform.h"

// Typedef component as parent
typedef Component Parent;

// Constructor
Transform::Transform(Entity* a_OwnerEntity)
	: Parent(a_OwnerEntity)
{
	// Set the component type
	m_eComponentType = TRANSFORM;
}

// Destructor
Transform::~Transform()
{
}
