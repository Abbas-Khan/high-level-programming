Abbas Khan - CT5037 High Level Programming Notice

- Project Details
Target Platform Version: 8.1 
Platform Toolset: Visual Studio 2015 v140
Project Configuration: Debug, x64

- Audio
The audio used in this assignment is licensed under CC 3.0 and the details for this can be found below.

"Robot Blip Sound" Marianne Gagnon (SoundBible.com)
Licensed under Creative Commons: By Attribution 3.0 License
http://creativecommons.org/licenses/by/3.0/

"Lion Growling Sounds" Mike Koenig (SoundBible.com)
Licensed under Creative Commons: By Attribution 3.0 License
http://creativecommons.org/licenses/by/3.0/

"Tiger Growling Sounds" Mike Koenig (SoundBible.com)
Licensed under Creative Commons: By Attribution 3.0 License
http://creativecommons.org/licenses/by/3.0/

"Babylon" Kevin MacLeod (incompetech.com)
Licensed under Creative Commons: By Attribution 3.0 License
http://creativecommons.org/licenses/by/3.0/